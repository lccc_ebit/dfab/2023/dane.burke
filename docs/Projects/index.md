# Projects/Assigments for Digital Fabrication of Elecromechanical Systems
## Start building a website with Git
### Git
 "[Git](https://git-scm.com/) is a free and open source distributed version control system."  This is how we build our website.
### Process
1. **GitLab**: Here at LCCC, we use GitLab for our repositories (repos).  It is basically a host for static websites that allow version control for single or multiple users whereby forks and brances of code can be utilized, experimented with, and implemented without affecting other versions before being merged to the master branch. 
2. Go to the [GitLab Website](https://gitlab.com/)
3. **Clone** the repo: I had to clone the repository that was created by my instructor, Chris Rohal, using a blank Fab Academy template.  This allows me to work locally from my computer and make changes to my repo (website). 
    - Here’s what the page looks like.  My repo is under LCCC_EBIT/DFAB/2023/Dane.Burke
    ![gitlab1](../images/wGitlab-1.png)

    - When I click on *code* (highlighted in pink above), it brings up the cloning options.  We choose “Clone with SSH” to ensure secure data transmission.
    ![gitlab2](../images/wGitlab-2.png)

    - Cloning gives me the following files:
    ![gitlab3](../images/wGitlab-3.png)

    - The highlighted “docs” folder is the main one I’ll be working from. It contains the following subfolders:
     ![gitlab4](../images/wGitlab-4.png)

 4.	I created a folder called DFAB221 on my desktop for the files and folders of the repo. I also created DFAB221_test folder with copies of everything. This is done in order to “play around” and test out changes without worry of “breaking” anything.  HoneyBun is watching to be make sure that I get my work done on time. 
  ![gitlab5](../images/Gitlab-5.png)

5. We were given the choice to use HTML or **MK Docs** programming language to build our website. Because I used HTML last year and because MK Docs has a cleaner UI, I chose MK Docs. MK Docs is a program/app that uses markdown language to build static websites. You just set it up by choosing a theme and the site pretty much does the rest by creating navigable tabs correspoinding to your main headers. 
    - [MK Docs](https://www.mkdocs.org/)
    - [Python](https://www.python.org/) has to be installed in order for MK Docs to work.
    - Here's a mark down language cheat sheet:
    ![gitlab5](../images/wMD-cheat.png)
    - Here's the mkdocs.yml file where all initial settings are:
    ![mkdocsyml](../images/MKDocsYML.png)

6. Install [Git](https://gitlab.com/). Note the pink highlights where I clicked on *downloads* and then chose the *64 bit Windows* option
     ![gitlab6](../images/wGitlab-6.png)
    - The Git download comes with **Git Bash**, Git CMD, and Git GUI. Git Bash is a “miniature version” of Linux that we will use for command prompts. Git CMD would be another choice to enter commands. I don’t know about Git GUI yet. 
    - Here’s the interface window for Git Bash (not much to see yet):
	![gitlab7](../images/wGitlab-7.png)

7. **Git Bash** commands
    - You must either choose the directory you want to work in by typing *$ CD (and then entering desired folder/subfolder)* **OR** From the desired directory you can right click, choose *More Options* and then select *Open Git Bash Here*
    - **Git status**: shows the current state of your Git working directory and staging area. More on [Git Status](https://github.com/git-guides/git-status)
    - **Git pull**: updates your current local working branch, and all of the remote tracking branches. More on [Git pull](https://github.com/git-guides/git-pull)
    - **Git add**: adds new or changed files in your working directory to the Git staging area. More on [Git add](https://github.com/git-guides/git-add)
    - **Git commit**: creates a commit, which is like a snapshot of your repository. Adding a -m allows you to add a message of your choosing to keep things straight. More on [Git commit](https://github.com/git-guides/git-commit)
    - **Git push**: uploads all local branch commits to the corresponding remote branch. More on [Git push](https://github.com/git-guides/git-push)

8. Once you *Git push* changes are **merged** to the main branch. 

9. Then you go to your MK Docs project and from the tree on the left select *Deploy* and then *Pages*:

    ![gitlab8](../images/wGitlab-8.png)


10. Finally you will get this page that has your website address (highlighted in pink). Be patient. It can take several seconds to reflect the changes, depending upon how much data is being added, changed, etc.

    ![gitlab-9](../images/wGitlab-9.png)
    

## Program a microcontroller
### For this class we use the **Seeed Studio XIAO RP2040 with Arduino**, which is about the size of my thumb (you wouldn't think a USB-C would fit it...but it does).

![RP-1](../images/wRP-1.jpg)

1. Go to [Seeed Wiki - Getting Started](https://wiki.seeedstudio.com/XIAO-RP2040/) to learn all the features and specs of the board.

2. Go to [Seeed Wiki with Arduino](https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/) and choose the options that are already hightlighted for us in green. 

    ![RP-2](../images/w.RP-2.png)

3. Follow the instructions to set up the board. This site has such good instrucitons and pictures that it wouldn't make a lot of sense for me to copy and paste them here. 

4. Part of the instructions mentioned above include downloading the [Arduino IDE Software](https://www.arduino.cc/en/software).

5. Open the **Arduino IDE**, select *File*, and then *Examples* to find some cool example sketches. 

6. First, to make sure the board was operating and all the LEDs were blinking etc. ran this sketch:

    ![RGB1](../images/RGB1.png)
    ![RGB2](../images/RGB2.png)

7. Next, I chose *Examples>01.Basics>Blink* to get started. Here's that very basic sketch. It's fun to play around with the parameters. 

    ![RP-3](../images/wRP-3.png)

    - From these, I learned about the **Void Setup** command that "sets up" each sketch and runs once. This is followed by the **Void Loop** command that runs over and over again.  
    - I also played around with another *Examples>02.Digital>BlinkWithoutDelay
    - Alas, it was time to move on to bigger things...


## Building a custom board
### Easy EDA
1. The first order of business here is to download [EasyEDA](https://easyeda.com/) which is where we design our PCB (printed circuit board).
2. Once downloaded, choose *File>New>Project* and then name and save the project. 

3. A blank **Sheet** interface will come up. From the menu on the left, choose *Commonly Library* which will  bring up several options for chips and components that can be dragged onto the blank sheet.    

    ![wEasy-1](../images/wEasy-1.png)

4. **We** chose the following layout with 2 pushbutton switches, 2 LEDs, and 2 resistors. 

    ![wEasy-2](../images/wEasy-2.png)

5. Choose *Wiring tool>Wire(W)* to make all the connections (the green lines seen above)
    - D0 to S1  
    - D1 to S2
    - D2 to LED 1
    - D3 to LED 2
    - All connected to ground

6. From the top menu, choose *Design>Convert Schematic to PCB*. The result will look something like this:

    ![wEasy-3](../images/wEasy-3.png)

7. All the components must be dragged to fit into the purple box, which is the size of your blank board to be cut. The blue lines will show you how, but not where the traces need to be connected. 

8. Next, chooose *PCB tools>Track(W*)* to make your traces. This is where it get tricky to route everything. If you left click on a component, you can rotate it, which makes routing the traces much easier. (As always, note the pink highlights.)

    ![wEasy-5](../images/wEasy-5.png)
   
   - You may notice something wrong. More on that later!

9. Last step here: choose *File>Export>PNG (or SVG)* for our next process. 

    ![wEasy-7](../images/wEasy-7.png)


### Mods Project
This is some crazy looking flowchart developed by Neil Gershenfeld at MIT's Fab Academy that uses PNG or SVG picture files to control CNC machines. 

1. Go to [Mods Project](https://modsproject.org/). Don't wait for the rest of the page to load, that's it. Just right click (anywhere) and choose *programs>open program>Roland>MDX mill>PCB*

    ![wMods-1](../images/wMods-1.png)

2. Then it goes from overly simple to mind-bogglingly complex.

    ![wMods-2](../images/wMods-2.png)

3. Left click and drag to move around the page. Zoom in on one of the first two boxes to select your picture file. 

    ![wMods-3](../images/wMods-3.png)

4. In pink (above), I highlighted the changes I made to the default settings.
    - The picture colors need inverted so the cuts follow the traces
    - Our cut depth on the 1/64th endmill is .002 in with a max depth of .008 in, which means it will make 4 passes with each with a lower z depth

5. For the mill, we choose *MDX-40* because our mill, the MDX-50 is not available in this list.  We set our origin to *X:0 Y:0*.* And we turn on the outputs the go to *Save File* so that our paramters save automatically with changes/updates. 

    ![wMods-4](../images/wMods-4.png)

6. When you're all set, click *Calculate*. You'll end up with this image and a .rml file with your project name on it that you can **SneakerNet** over to the MDX50's computer with a USB drive.

    ![wMods-5](../images/wMods-5.png)

### Milling the board

Now for the fun part.  I finally get to use the Roland Modela MDX-50.  A nifty little machine with a straight-forward interface and best of all, an automatic tool changer.  

1. **Tape** down the blank board to the sacraficial board with double-sided tape. Three strips because these boards are kind of bowed. 

    ![Mill-1](../images/wMill-1.jpg)

    ![Mill-2](../images/wMill-2.jpg)

2. The *Menu* button cycles through the necessary screens and *Enter* makes the selections. 

    ![Mill-6](../images/wMill-6.jpg)

3. Select *Tool 1* (the 64th endmill that we will be using to cut the traces) and zero out the X and Y axes. 

    ![Mill-3](../images/wMill-3.jpg)

4. Once you have your mark, press and hold the *Origin* button at X and Y to zero.

    ![Mill-4](../images/wMill-4.jpg)

5. Select *Tool 6*, the Z sensor tool. 

    ![Mill-5](../images/wMill-5.jpg)

6. *Plug in the Z sense thing* and place on your board (high spot if there is one).

    ![Mill-7](../images/wMill-7.jpg)

7. Press the *Z0 Sense button* and then confirm. 

    ![Mill-8](../images/wMill-8.jpg)

8. Switch back to Tool 1 and start *warming up the spindle* at the lowest speed (4500).  

    ![Mill-9](../images/wMill-9.jpg)

9. Go to the computer and click the **VPanel for MDX icon** to open the control interface.

    ![Mill-10](../images/wMill-10.jpg)

10. Select *Cut>Add* and find load your file from the USB drive. (Don't forget to ramp up the spindle speed during the time you're at the computer).

    ![Mill-11](../images/wMill-11.jpg)

    ![Mill-12](../images/wMill-12.jpg)

11. Once you've got your spindle speed to 15,000, double check that your tool is in place and your board hasn't popped up. Click **Cut**.

    ![Mill-13](../images/wMill-13.jpg)

12. Try **NOT** to freak out when the spindle looks like it's going to clip your board when it moves into place.  Enjoy the cut. 

    ![Mill-14](../images/wMill-14.jpg)

13. Hopefully yours will go better than my first couple ones. 

    - This one cut too deep.  May have been from a bowed board. May have been from a broken endmill. Or it may have broken the endmill during the deep cut. Regardless, we had the replace the endmill. 

    ![Mill-17](../images/wMill-17.jpg)

    - This one didn't cut deep enough so we had to adjust our settings in Mod Project (to the ones listed there). 

    ![Mill-15](../images/wMill-15.jpg)

14. Finally got a good cut and after soldering on the LEDS and resistors, we realized the push button switch pads did not match our push button switches. Also, my instructor noticed the push button switch pads did not connect to ground.  Upon further investigation, we found that the traces on the Easy EDA Sheet did not fully connect to the ground pin so when it was converted to the PCB it did not show the leads and I did not notice.  After fixing the sheet, I was able to at least have a good PCB image for future use (2nd pic).

    ![Mill-16](../images/wMill-16.jpg)

    ![Easy-6](../images/wEasy-6.png)

15. Both LEDs light up (yay!) and I had fun playing around with some more arduino sketches. 

    - [Blink.ino](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink)

16. We remedied the pushbutton situation by soldering a wire from the PB pad to ground and adding one of these guys:

    ![Mill-18](../images/wMill-18.jpg)

17. Which worked great until the it lifted the trace up because the copper on these boards is paper thin. 

    ![Mill-19](../images/wMill19.jpg)

18. Here is the cut board connected (mostly) to he microcontroller. 

    ![Mill-20](../images/wMill-20.jpg)




