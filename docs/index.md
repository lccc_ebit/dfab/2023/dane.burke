# Home
 Welcome to my website for the Digital Fabrication Progam at Lorain County Community College (LCCC).  Check back here on occasion to see updates on my projects. LCCC is located in Elyria, OH. They have great technology programs and facilities. When I decided to go back to school, I was interested in their automation program. But then I discovered the FabLab...

## FabLab
[FabLab](https://www.lorainccc.edu/campana/fab-lab)

- LCCC's FabLab is a 5000 square foot facility located in the Campana Center for Ideation and Invention.  They have several routers including full size, destop, and mini, several lasers for cutting and engraving, a vinyl cutter, several 3D printers, and several computers with the software to create the designs and control the machines.  It is open to the public. Definitely check out their website for updates and all the details. 
- Backstory: A number of years ago I stumbled upon a company that started out in California called TechShop. [Wikipedia on TechShop](https://en.wikipedia.org/wiki/TechShop) It was a membership based business that had every imaginable machine to design and make things.  With a membership and safety training, you could use any machine in the facility. They ended up spreading and had 10 locations across the country (with Detroit being the closest).  I visited a couple of them and was super excited about the opportunity to take part. Unfortunately for me, I didn't live close enough to make the reasonable, but not so cheap, membership cost worth it.  Unfortunetly for TechShop (and, imo, the world) they closed without warning in 2017. I don't know the details, but my guess is that memberships didn't sell as they had hoped and they couldn't defray the exorbitant costs of the multi-million dollar equipment they owned and maintained. So, even though FabLab is a miniature version of what TechShop was, I was super excited to learn there was one in our back yard.  So, it was a no brainer for me to go for the Associate's degree in Digital Fabrication. 

## LCCC's Digital Fabrication Program
- The Digital Fabrication (DFAB) degree is a 2 year full time program leading to an Associate of Applied Science Degree. It is based on MIT's FabAcademy, which covers the same material over 20 weeks in an intensive bootcamp style format. [FabAcademy](https://fabacademy.org/)
- [LCCC DFAB Program overview](http://catalog.lorainccc.edu/academic-programs/engineering-business-information-technologies/digital-fabrication-technologies-digital-fabrication-major-aas/#text)
- [DFAB Curriculum Guide](http://catalog.lorainccc.edu/academic-programs/engineering-business-information-technologies/digital-fabrication-technologies-digital-fabrication-major-aas/#curriculumguidetext)


