# About me
## My Background
 - I am a Northern Ohio native. Born and raised in Huron County in a small town on the mighty Vermilion River. 

 ![tressel](../images/Tressel.jpg)

 - I have a BA in Psychology from Miami University (Go Red Hawks!)
 - I have an MS in Physical Therapy from Washington University School of Medicine 

 - This is me with my parents on their 50th anniversary in August of 2014.  My Dad passed in June of this year(2023) and is greatly missed all the moments of each day. 
![Fam](../images/wFam.jpg)




## My interests
- My 2 dogs: HoneyBun and Bear 
![BearandBun](../images/wHB-Bear.jpg)

    This is Bear. She is the mama. She is half Shi-Tzu and half Feist.
![Bear](../images/wBear.jpg)

    This is HoneyBun aka Bun. She is Bear's daughter. She is half "Bear" and half Pug.
![This is HoneyBun aka Bun. She is the Bear's daughter. She is half "Bear" and half Pug](../images/wBun.jpg)

- My newly acquired cat: RavenPaw. He's full of fire and always quite busy. 
![Raven. He is full of fire](../images/wRaven.jpg)

- Making, building, creating, tinkering, inventing, designing

- Skiing (especially around Lake Tahoe)

- Riding Motorcycles (especially through Big Sur)

- RVs (especially toy haulers and custom builds)

- Traveling (the Canadian Rockies and Ecuador have been my favorite places so far)


 


 



